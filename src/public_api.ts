/*
 * Public API Surface of dining
 */

export * from './lib/dining.service';
export * from './lib/dining.component';
export * from './lib/dining.module';
export { DINING_LOCALES } from './lib/i18n/index';