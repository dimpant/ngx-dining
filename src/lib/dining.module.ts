import { NgModule, OnInit, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdvancedFormsModule } from '@universis/forms';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';
import { DiningComponent } from './dining.component';
import { DiningRoutingModule } from './dining-routing.module';
import {DINING_LOCALES} from './i18n';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DiningRoutingModule,
    FormsModule,
    AdvancedFormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    TabsModule.forRoot(),
    NgArrayPipesModule,
    NgxDropzoneModule      
  ],
  declarations: [DiningComponent],
  exports: [DiningComponent]
})
export class DiningModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: DiningModule,
              private _translateService: TranslateService) {
    if (parentModule) {
      // throw new Error(
      //    'DiningModule is already loaded. Import it in the AppModule only');
    }
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading dining module');
      console.error(err);
    });
  }

  async ngOnInit() {
    Object.keys(DINING_LOCALES).forEach( language => {
      if (DINING_LOCALES.hasOwnProperty(language)) {
        this._translateService.setTranslation(language, DINING_LOCALES[language], true);
      }
    });
  }
}