import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiningComponent } from './dining.component';

const routes: Routes = [
  {
    path: 'apply',
    component: DiningComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DiningRoutingModule { }
