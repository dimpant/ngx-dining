import { Component, OnInit, OnDestroy, Input, ViewChild, AfterViewInit, EventEmitter, Output, OnChanges, SimpleChanges, ChangeDetectorRef, SimpleChange, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedUser, LoadingService, ErrorService, UserService, ModalService, DIALOG_BUTTONS } from '@universis/common';
import { Subscription, forkJoin, combineLatest } from 'rxjs';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { AdvancedFormComponent } from '@universis/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseError } from '@themost/client';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep, template } from 'lodash';

@Component({
  selector: 'app-dining',
  templateUrl: './dining.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./dining.component.scss']
})
export class DiningComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

  constructor(private _context: AngularDataContext,
              private _userService: UserService,
              private _loading: LoadingService,
              private _errorService: ErrorService,
              private _activatedRoute: ActivatedRoute,
              private _http: HttpClient,
              private _modal: ModalService,
              private _translateService: TranslateService,
              private _router: Router,
              private cd: ChangeDetectorRef) { }

  /**
   * Returns the current model state (for insert or update)
   */
  get modelState(): string {
    if (this.model && typeof this.model.id === 'number') {
      return 'update';
    }
    return 'insert';
  }

  get documentState(): string {
    if (this.documents == null) {
      return 'invalid';
    }
    const required = this.documents.filter((item) => {
      return item.numberOfAttachments > 0 && item.attachment == null;
    }).length;
    if (required > 0) {
      return 'invalid';
    }
    return 'valid';
  }

  get activeTab(): any {
    return this.tabset.tabs.find((tab) => {
      return tab.active;
    });
  }
  queryParamSubscription: Subscription;

  public showNewMessage = false;
  public newMessage: { subject?: string; body?: string; attachments: any[]; } = {
    attachments: []
  };
  public messages: any[];
  public nextButtonDisabled = true;
  public documentStateChange: EventEmitter<any> = new EventEmitter<any>();
  public documents: { attachment: any; attachmentType: any; numberOfAttachments: number }[];
  
  public formAction: string;

  public diningRequestEvent: any;

  @Input() model: any = {
    attachments: [],
    actionStatus: {
      alternateName: 'PotentialActionStatus'
    }
  };
  @ViewChild('tabset', {
  }) tabset: TabsetComponent;
  @ViewChild('form1', {
  }) form1: AdvancedFormComponent;
  @ViewChild('attachDocuments', {
  }) attachDocuments: TabDirective;
  ngOnChanges(changes: SimpleChanges): void {
    //
  }
  ngOnDestroy(): void {
    if (this.queryParamSubscription) {
      this.queryParamSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {

    this.queryParamSubscription = combineLatest(
      this._activatedRoute.queryParams,
      this._activatedRoute.params
    ).subscribe((results) => {
      const queryParams = results[0];
      const params = results[1];
      // validate code
      if (queryParams.code == null) {
        // loading
        this._loading.showLoading();
        // prepare new item
        Promise.all([
          this._userService.getUser(),
          this._context.model('students/me')
            .asQueryable()
            .expand('user', 'department', 'studyProgram', 'inscriptionMode', 'person').getItem()
        ]).then((results1) => {                    
          const user = results1[0];                  
          const student = results1[1];
          // get dining request event              
          Promise.all([
            (this._context.model('DiningRequestEvents').where('academicYear').equal(student.department.currentYear)
            .and('academicPeriod').equal(student.department.currentPeriod)
            .expand('attachmentTypes($expand=attachmentType)')
            .getItem())
          ]).then((results2) => {
            this.diningRequestEvent = results2[0];
          });
          // set form
          this.formAction = params.action ? `dining/dining-form` : `dining/dining-form`;
          // and continue
          this._loading.hideLoading();
        }).catch((err) => {
          this._loading.hideLoading();
          return this._errorService.navigateToError(err);
        });        


      } else {
        this._loading.showLoading();
        Promise.all([
          this._context.model('DiningRequestActions').where('code').equal(queryParams.code)
            .expand('attachments($expand=attachmentType)')
            .getItem()
        ]).then((results1) => {
          debugger;
          const item = results1[0];
          if (item == null) {
            // tslint:disable-next-line:max-line-length
            return this._errorService.navigateToError(new ResponseError('The specified application cannot be found or is inaccessible', 404.5));
          }
          this.model = item;
          // set form
          this.formAction = params.action ? `dining/dining-form` : `dining/dining-form`;
          this.nextButtonDisabled = false;
          // fetch messages
          this.fetchMessages();
          this._loading.hideLoading();
        }).catch((err) => {
          this._loading.hideLoading();
          return this._errorService.navigateToError(err);
        });
      }
    });
  }

  fetchMessages(): void {
    if (this.model && this.model.id) {
      this._context.model(`DiningRequestActions/${this.model.id}/messages`).asQueryable()
        .orderBy('dateCreated desc').expand('attachments').getItems().then((messages) => {
          this.messages = messages;
        }).catch((err) => {
          console.error(err);
          this.messages = [];
        });
    } else {
      this.messages = [];
    }
  }

  ngAfterViewInit(): void {
    //
  }

  onDataChange(event: any): void {
    if (this.form1 && this.form1.form) {
      // enable or disable button based on form status
      this.nextButtonDisabled = !this.form1.form.formio.isValid();
    }
  }

  async beforeNext(nextTab: TabDirective): Promise<boolean> {

    try {
      if (nextTab.id === 'personal-information') {
        return true;
      }
      // save application before attach documents
      if (nextTab.id === 'attach-documents') {
        // save action
        if (this.model.actionStatus.alternateName === 'PotentialActionStatus') {
          this._loading.showLoading();
          // set additional data
          const data = this.form1.form.formio.data;
          Object.assign(data, {
            agent: null
          });
          // save application
          const result = await this._context.model('DiningRequestActions').save(data);
          // update current action
          Object.assign(this.model, result);
          // and continue
          this._loading.hideLoading();
        }
      }
      return true;
    }
    catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
      return false;
    }
  }

  next(): void {
    // get active tab
    const findIndex = this.tabset.tabs.findIndex((tab) => {
      return tab.active;
    });
    if (findIndex < this.tabset.tabs.length) {
      // set active tab
      const tab = this.tabset.tabs[findIndex + 1];
      this.beforeNext(tab).then((result) => {
        if (result) {
          tab.disabled = false;
          tab.active = true;
        }
      });
    }
  }

  validateEnrollmentPeriod(enrollmentEvent: { validFrom?: Date; validThrough?: Date }): void {
    const now = new Date();
    let valid = false;
    let started = false;
    if (enrollmentEvent.validFrom instanceof Date) {
      if (enrollmentEvent.validThrough instanceof Date) {
        valid = enrollmentEvent.validFrom <= now && enrollmentEvent.validThrough > now;
      } else {
        valid = enrollmentEvent.validFrom < now;
      }
      if (enrollmentEvent.validFrom <= now) {
        started = true;
      }
    } else if (enrollmentEvent.validThrough instanceof Date) {
      valid = enrollmentEvent.validThrough > now;
      if (enrollmentEvent.validThrough > now) {
        started = true;
      }
    }
    Object.assign(enrollmentEvent, {
      valid,
      started
    });
  }

  onFileSelect(event, attachmentType): any {
    // get file
    const addedFile = event.addedFiles[0];
    const formData: FormData = new FormData();
    formData.append('file', addedFile, addedFile.name);
    formData.append('attachmentType', attachmentType.id);
    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/addAttachment`);
    this._loading.showLoading();
    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    }).subscribe((result) => {
      // reload attachments
      this._context.model(`DiningRequestActions/${this.model.id}/attachments`)
        .asQueryable().expand('attachmentType')
        .getItems()
        .then((attachments) => {
          // set model attachments
          this.model.attachments = attachments;
          // refresh attachment
          this.onSelectAttachDocuments();
          // hide loading
          this._loading.hideLoading();
        }).catch((err) => {
          window.location.reload();
          this._loading.hideLoading();
        });
    }, (err) => {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  onFileRemove(attachment: any): any {
    this._loading.showLoading();
    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/removeAttachment`);
    return this._http.post(postUrl, attachment, {
      headers: serviceHeaders
    }).subscribe(() => {
      // reload attachments
      this._context.model(`DiningRequestActions/${this.model.id}/attachments`)
        .asQueryable().expand('attachmentType')
        .getItems()
        .then((attachments) => {
          // set model attachments
          this.model.attachments = attachments;
          // refresh attachment
          this.onSelectAttachDocuments();
          // hide loading
          this._loading.hideLoading();
        }).catch((err) => {
          window.location.reload();
          this._loading.hideLoading();
        });
    }, (err) => {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  onSelectAttachDocuments(): void {
    this.documents = this.documents || [];
    // if study program enrollment event is not not defined
    if (this.diningRequestEvent == null) {
      // exit
      return;
    }
    this.documents = this.diningRequestEvent.attachmentTypes.map((item: any) => {
      const res = {
        attachmentType: item.attachmentType,
        numberOfAttachments: item.numberOfAttachments,
        validationExpression: item.validationExpression
      };
      // try to search model attachments list
      const findAttachment = this.model.attachments.find((attachment) => {
        if (attachment.attachmentType == null) {
          return false;
        }
        return attachment.attachmentType.id === item.attachmentType.id;
      });
      if (findAttachment) {
        // assign data
        Object.assign(res, {
          attachment: findAttachment
        });
      }
      return res;
    }).filter(item => { 
        if (item.validationExpression) {
          item.numberOfAttachments = template(item.validationExpression)(this.model) === true ? item.numberOfAttachments : 0;
          if (item.numberOfAttachments == 0) {
            return false;
          }
        }
        return true;
    });
    this.documentStateChange.emit(this.documentState);
  }

  submit(): any {
    const SubmitTitle = this._translateService.instant('SubmitTitle');
    const SubmitTitleMessage = this._translateService.instant('SubmitTitleMessage');
    this._modal.showSuccessDialog(SubmitTitle, SubmitTitleMessage).then((result) => {
      if (result === 'ok') {
        this._loading.showLoading();
        const model = cloneDeep(this.model);
        // remove attachments
        delete model.attachments;
        // set action status
        model.actionStatus = {
          alternateName: 'ActiveActionStatus'
        };
        this._context.model('DiningRequestActions').save(model).then(() => {
          // navigate to list
          this._loading.hideLoading();
          return this._router.navigate([
            '/'
          ]);
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  onSelectTab(event: any): void {
    //
  }

  downloadAttachment(attachment: any): void {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    this._loading.showLoading();
    fetch(fileURL, {
      headers,
      credentials: 'include'
    }).then((response) => {
      if (response.status !== 200) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
        this._loading.hideLoading();
      }).catch((err) => {
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }


  async send(message: any): Promise<void> {
    try {
      this._loading.showLoading();
      // set recipient (which is this action owner)
      Object.assign(message, {
        recipient: {
          name: 'Registrar'
        }
      });
      await this._context.model(`DiningRequestActions/${this.model.id}/messages`).save(message);
      // reload message
      this.fetchMessages();
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

}