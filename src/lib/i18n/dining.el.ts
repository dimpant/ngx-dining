/* tslint:disable max-line-length */
export const el = {
  NewRequestTemplates: { 
    DiningRequestAction: {
      "Name": "Αίτηση Σίτισης",
      "Title": "Αίτηση Σίτισης",
      "Summary": "Αίτηση Σίτισης"
    }
  },
  UniversisDiningModule: {    
    PersonalInformation: 'Προσωπικά Στοιχεία',
    DiningDocuments: 'Δικαιολογητικά',
    DiningRequestTitle: 'Αίτηση Σίτισης',
    DiningRequestCapitalTitle: 'ΑΙΤΗΣΗ ΣΙΤΙΣΗΣ',
    DocumentsSubmission: {
      Title: 'Κατάθεση εγγράφων',
      Subtitle: 'Ακολούθησε τις παρακάτω οδηγίες για να καταθέσεις τα απαιτούμενα έγγραφα για την αίτηση σίτισής σου.',
      SubmissionStatus: 'Κατάσταση κατάθεσης',
      SubmissionStatuses: {
        pending: 'Η κατάθεση εγγράφων για την σίτιση είναι σε εξέλιξη',
        completed: 'Η κατάθεση εγγράφων για την σίτιση έχει ολοκληρωθεί',
        failed: 'Η κατάθεση εγγράφων για την σίτιση είναι σε εξέλιξη',
        unavailable: 'Η κατάθεση εγγράφων δεν είναι διαθέσιμη'
      },
      AttachmentDeleteModal: {
        Title: 'Διαγραφή εγγράφου',
        Body: 'Διαγραφή εγγράφου τύπου {{attachmentType}};',
        Notice: 'Αυτή η πράξη δεν είναι αναιρέσιμη.',
        Close: 'Κλείσιμο',
        Delete: 'Διαγραφή'
      },
      DiningDocumentsToUpload: 'Έγγραφα σίτισης για μεταφόρτωση',
      DiningDocumentsPhysicals: 'Έγγραφα σίτισης που πρέπει να παραδοθούν στην γραμματεία του τμήματος',
      DownloadDocument: 'Λήψη εγγράφου',
      UploadDocument: 'Μεταφόρτωση',
      RemoveDocument: 'Αφαίρεση αρχείου',
      ContactService: 'Επικοινωνία με υπεύθυνο',
      Errors: {
        Download: 'Υπήρξε σφάλμα κατά την λήψη του αρχείου',
        Remove: 'Υπήρξε σφάλμα κατά την αφαίρεση του αρχείου',
        Upload: 'Υπήρξε σφάλμα κατά την μεταφόρτωση του αρχείου'
      }
    },
    ModalConfirm: {
      Submit: 'Ολοκλήρωση',
      Close: 'Κλείσιμο',
      Title: 'Αποστολή Αίτησης Σίτισης',
      Body: 'Θέλετε να στείλετε την αίτηση σας για αποστολή και έλεγχο στην γραμματεία;'
    },
    MessagePrompt: 'Το μήνυμά σου',
    Send: 'Αποστολή',
    Cancel: 'Ακύρωση',
    Date: 'Ημερομηνία',
    Time: 'Ώρα',
    Location: 'Τοποθεσία',
    Download: 'Λήψη',
    Previous:  'Προηγούμενο',
    Next: 'Επόμενο',
    Completed: 'Ολοκλήρωση Αίτησης',
    ContactRegistrar: 'Επικοινωνία με Γραμματεία',
    StudentInfo: 'Στοιχεία Φοιτητή',
    StudyGuide: 'ΟΔΗΓΟΣ ΣΠΟΥΔΩΝ',
    Specialty : 'ΚΑΤΕΥΘΥΝΣΗ',
    Prerequisites: 'Προϋποθέσεις',
    Progress: 'Πρόοδος',
    NoRulesFound: 'Οι προϋποθέσεις πτυχίου δεν έχουν οριστεί.',
    CourseType: 'Τύπος Μαθημάτων',
    AllTypeCourses: 'Όλοι οι τύποι μαθημάτων',
    Thesis: 'Εργασία',
    Student: 'Ιδιότητες Φοιτητή',
    Internship: 'Πρακτική',
    Course: 'Προαπαιτούμενο Μάθημα',
    CourseArea: 'Γνωστικό Αντικείμενο',
    CourseCategory: 'Κατηγορία Μαθήματος',
    CourseSector: 'Τομέας Μαθημάτων',
    ProgramGroup: 'Ομάδα Μαθημάτων',
    StatusLabel: 'Κατάσταση της αίτησης σου',
    NoAttachments: 'Δεν υπαρχουν έγγραφα για μεταφόρτωση'
  }
};
