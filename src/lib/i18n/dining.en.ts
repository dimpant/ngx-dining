/* tslint:disable max-line-length */
export const en = {
  NewRequestTemplates: {   
    DiningRequestAction: {
      "Name": "Dining Request",
      "Title": "Dining Request",
      "Summary": "Dining Request"
    }      
  },
  UniversisDiningModule: {
    PersonalInformation: 'Personal Information',    
    DiningRequestTitle: 'Dining request',
    DiningRequestCapitalTitle: 'DINING REQUEST',
    DocumentsSubmission: {
      Title: ' Documents submission',
      Subtitle: ' Follow the instructions in order to submit the required documents for your dining request.',
      SubmissionStatus: 'Submission status',
      SubmissionStatuses: {
        pending: 'Document submission for dining is ongoing',
        completed: 'Document submission for dining is completed',
        failed: 'Document submission for dining is ongoing',
        unavailable: 'Document submission is not available'
      },
      AttachmentDeleteModal: {
        Title: 'Document delete',
        Body: 'Delete document of {{attachmentType}} type?',
        Notice: 'This action can not be undone.',
        Close: 'Close',
        Delete: 'Delete'
      },
      DiningDocumentsToUpload: 'Dining documents for upload',
      DiningDocumentsPhysicals: 'Dining documents to be delivered to the secretariat of the department',
      DownloadDocument: 'Download document',
      UploadDocument: 'Upload document',
      RemoveDocument: 'Remove document',
      ContactService: 'contact related service',
      Errors: {
        Download: 'There was an error during the file download',
        Remove: 'There was an error during the file removal',
        Upload: 'There was an error during the file upload'
      }
    },
    ModalConfirm: {
      Submit: 'Submit',
      Close: 'Close',
      Title: 'Send Dining Request',
      Body: 'You want to send your request for sending and check to the secretariat?'
    },
    MessagePrompt: 'Your message',
    Send: 'Send',
    Cancel: 'Cancel',
    Date: 'Date',
    Time: 'Time',
    Location: 'Location',
    Download: 'Download',
    Previous:  'Previous',
    Next: 'Next',
    Completed: 'Request Completion',
    ContactRegistrar: 'Contact Registrar',
    StudentInfo: 'Student Information',
    StudyGuide: 'STUDY GUIDE',
    Specialty : 'SPECIALTY',
    Prerequisites: 'Prerequisites',
    Progress: 'Progress',
    NoRulesFound: 'Dining Rules have not been set.',
    CourseType: 'Course Type',
    AllTypeCourses: 'All Type Courses',
    Thesis: 'Thesis',
    Student: 'Semester',
    Internship: 'Internship',
    Course: 'Prerequisite Courses',
    CourseArea: 'Courses Area',
    CourseCategory: 'Courses Category',
    CourseSector: 'Courses Sector',
    ProgramGroup: 'Courses Group',
    StatusLabel: 'Status of your request',
    NoAttachments: 'There are no attachments for upload'
  }
};
